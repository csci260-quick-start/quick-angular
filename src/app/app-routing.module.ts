import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LinkPageComponent } from './link-page/link-page.component';

const routes: Routes = [
  { path: 'link', component: LinkPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
