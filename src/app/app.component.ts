import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User, DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'quick-angular';
  users: User[] = [];
  logContent: string[];
  private x: number = 1;

  constructor(private router: Router, private dataService: DataService) {}

  ngOnInit() {
    this.dataService.getData().subscribe(
      users => this.users = users
    );
  }

  doLink(): void {
    this.router.navigate(['/link']);
  }

  demo(): void {
    const logs = ['1 + 2 = ' + (1 + 2)];
    logs.push('\'1\' + 2 = ' + ('1' + 2));
    logs.push('1 + \'2\' = ' + (1 + '2'));

    for (let i = 0; i < 10; i++) {
      this.x += i;
    }
    logs.push('x is ' + this.x);
    // this.x = 'blue';
    // logs.push('x is ' + this.x);

    this.logContent = logs;
  }
}
