import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../environments/environment';

const server = environment.server;

export interface User {
  first_name: string;
  last_name: string;
}

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private http: HttpClient) { }

  getData(): Observable<User[]> {
    return this.http.get<User[]>(server + '/users');
  }
}
